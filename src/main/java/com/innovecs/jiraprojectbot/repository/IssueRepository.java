package com.innovecs.jiraprojectbot.repository;

import com.innovecs.jiraprojectbot.model.JIssue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface IssueRepository extends JpaRepository<JIssue, Integer> {

    @Query("select j " +
            "from JIssue j " +
            "where (j.updated > '2020-12-31' or j.status not in ('Resolved', 'Closed', 'Done')) and j.projectKey = ?1")
    //It was changed according to request CORPSYS-3222 (https://innovecs.atlassian.net/browse/CORPSYS-3222)
    List<JIssue> findAllByProjectKey(String project);

    List<JIssue> findAllByProjectKeyAndUpdatedAfterAndStatusIsNotIn(String project, LocalDateTime updated, List<String> statuses);

    void deleteAllByProjectKey(String project);

    List<JIssue> findAllByProjectKeyAndUpdatedAfter(String key, LocalDateTime date);
}
