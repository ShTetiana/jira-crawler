package com.innovecs.jiraprojectbot.repository;

import com.innovecs.jiraprojectbot.model.JChangelog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChangelogRepository extends JpaRepository<JChangelog, Integer> {
}
