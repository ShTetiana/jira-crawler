package com.innovecs.jiraprojectbot.repository;

import com.innovecs.jiraprojectbot.model.JProject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends JpaRepository<JProject, Integer> {

    Optional<JProject> findByProjectKey(String key);

    boolean existsByProjectKey(String key);

    List<JProject> findAllByReport(String report);

}
