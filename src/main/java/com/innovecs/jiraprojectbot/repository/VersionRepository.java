package com.innovecs.jiraprojectbot.repository;

import com.innovecs.jiraprojectbot.model.JVersion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VersionRepository extends JpaRepository<JVersion, Integer> {

    void deleteAllByProjectKey(String key);
}
