package com.innovecs.jiraprojectbot.repository;

import com.innovecs.jiraprojectbot.model.JUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<JUser, String> {
}
