package com.innovecs.jiraprojectbot.repository;

import com.innovecs.jiraprojectbot.model.JWorklog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorklogRepository extends JpaRepository<JWorklog, Integer> {

    void deleteAllByProjectKey(String key);
}
