package com.innovecs.jiraprojectbot.repository;

import com.innovecs.jiraprojectbot.model.links.VersionIssue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VersionIssueRepository extends JpaRepository<VersionIssue, Integer> {

    boolean existsByIssueIdAndLinkTypeAndVersionId(Integer issueId, String linkType, Integer versionId);
}
