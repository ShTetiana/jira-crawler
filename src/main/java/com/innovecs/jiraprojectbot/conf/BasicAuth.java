package com.innovecs.jiraprojectbot.conf;

import com.sun.istack.NotNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;


// -Dspring.profiles.active=dev
@Log4j2
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Order(0)
class BasicAuth extends WebSecurityConfigurerAdapter {
    @Value("${crawler.jira.password}")
    @NotNull
    private String pwd;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth

                .inMemoryAuthentication()
                .withUser("admin")
                .password(encoder.encode(pwd))
                .roles("admin");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .antMatcher("**")
                .authorizeRequests()
                .anyRequest()
                .hasRole("admin")
//                .and().formLogin()
//                .and().logout()
                .and()
                .httpBasic();
    }
}
