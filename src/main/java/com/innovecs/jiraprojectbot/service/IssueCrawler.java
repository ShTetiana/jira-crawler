package com.innovecs.jiraprojectbot.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecs.jiraprojectbot.model.JIssue;
import com.innovecs.jiraprojectbot.model.JProject;
import com.innovecs.jiraprojectbot.model.JVersion;
import com.innovecs.jiraprojectbot.model.dto.IssueWithLabels;
import com.innovecs.jiraprojectbot.model.dto.LabelDto;
import com.innovecs.jiraprojectbot.model.links.VersionIssue;
import com.innovecs.jiraprojectbot.repository.IssueRepository;
import com.innovecs.jiraprojectbot.repository.ProjectRepository;
import com.innovecs.jiraprojectbot.repository.VersionIssueRepository;
import com.innovecs.jiraprojectbot.repository.VersionRepository;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.sun.istack.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
@RequiredArgsConstructor
public class IssueCrawler {

    private final VersionIssueRepository versionIssueRepository;
    private final VersionRepository versionRepository;
    private final IssueRepository issueRepository;
    private final ObjectMapper objectMapper;

    private final ProjectRepository projectRepository;

    private static JProject jProject;

    @Value("${jira.auth.user}")
    @NotNull
    private String user;

    @Value("${jira.auth.pswd}")
    @NotNull
    private String pswd;

    public List<IssueWithLabels> getAllIssuesForReportByDateIsAfterAndStatusIsNotIn(String report, LocalDateTime isAfter, List<String> statuses ) {
        List<IssueWithLabels> dtos = new ArrayList<>();
        log.info("sd " + projectRepository.findAllByReport(report).size());
        for (JProject project : projectRepository.findAllByReport(report)) {
            dtos.addAll(getIssuesByProjectAndUpdatedAndStatusIsNotIn(project.getProjectKey(), isAfter, statuses));
        }

        return dtos;
    }

    public List<IssueWithLabels> getAllIssuesForReport(String report) {
        List<IssueWithLabels> dtos = new ArrayList<>();
        log.info("sd " + projectRepository.findAllByReport(report).size());
        for (JProject project : projectRepository.findAllByReport(report)) {
            dtos.addAll(getIssuesByProject(project.getProjectKey()));
        }

        return dtos;
    }

    public void crawlAllIssueByProject(String projectKey) {
        if (projectRepository.existsByProjectKey(projectKey)) {
            issueRepository.deleteAllByProjectKey(projectKey);
            crawlIssuesByProject(projectKey, "project=" + projectKey);
        } else {
            log.info("The project has not been found");
        }
    }

    public void crawlIssuesByProjectForDay(String projectKey) {

        LocalDate date = LocalDate.now().minusDays(2);
        crawlIssuesByProject(projectKey, "project=" + projectKey + " and updatedDate >= '" + date.toString() + "'");
    }

    private void crawlIssuesByProject(String projectKey, String jql) {

        Optional<JProject> optionalJProject = projectRepository.findByProjectKey(projectKey);

        if (optionalJProject.isPresent()) {
            jProject = optionalJProject.get();
            try {

                List<JIssue> jIssues = new ArrayList<>();

                int counter = 0;

                while (true) {

                    String url = "https://" + jProject.getDomain() + ".atlassian.net/rest/api/3/search";// + counter;
                    HttpResponse<String> response = Unirest.get(url)
                            .basicAuth(user, pswd)
                            .header("Accept", "application/json")
                            .queryString("jql", jql)
                            .queryString("startAt", counter)
                            .asString();
                    JsonNode parentNode = objectMapper.readTree(response.getBody()).get("issues");
                    if (parentNode.isArray()) {
                        if (parentNode.isEmpty()) {
                            log.info("Issues total size: " + issueRepository.count());
                            break;
                        }
                        log.info("Received: " + parentNode.size() + " records about Issues, count: " + counter);
                        for (JsonNode node : parentNode) {
                            JIssue jIssue = getIssue(node);
                            jIssues.add(jIssue);
                        }
                    }
                    counter += 50;
                    Thread.sleep(1000);
                }

                if (!jIssues.isEmpty()) {
                    issueRepository.saveAll(jIssues);
                    log.info(jIssues.size() + " issues are saved");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            log.warn("Project has not been found");
        }
        jProject = new JProject();
    }


    private JIssue getIssue(JsonNode node) {
        JIssue issue = new JIssue();

        issue.setSelfLink(node.get("self").textValue());
        issue.setId(Integer.parseInt(node.get("id").textValue()));
        issue.setKey(node.get("key").textValue());

        JsonNode fieldsNode = node.get("fields");

        issue.setPriority(fieldsNode.get("priority").get("name").textValue());
        issue.setPriorityId(Integer.parseInt(fieldsNode.get("priority").get("id").textValue()));

        List<String> labels = new ArrayList<>();
        if (!fieldsNode.get("labels").isEmpty())
            fieldsNode.get("labels").forEach(label ->
                    labels.add(label.textValue()));
        issue.setLabels(labels);

        issue.setAssigneeId(fieldsNode.get("assignee").get("accountId").textValue());

        issue.setStatus(fieldsNode.get("status").get("name").textValue());
        issue.setStatusId(Integer.parseInt(fieldsNode.get("status").get("id").textValue()));

        issue.setCreatorId(fieldsNode.get("creator").get("accountId").textValue());

        issue.setReporterId(fieldsNode.get("reporter").get("accountId").textValue());

        issue.setType(fieldsNode.get("issuetype").get("name").textValue());
        issue.setTypeId(Integer.parseInt(fieldsNode.get("issuetype").get("id").textValue()));

        issue.setProjectKey(fieldsNode.get("project").get("key").textValue());
        issue.setProjectId(Integer.parseInt(fieldsNode.get("project").get("id").textValue()));

        issue.setSummary(fieldsNode.get("summary").textValue());

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        issue.setUpdated(LocalDateTime.parse(fieldsNode.get("updated").textValue(), format));
        issue.setCreated(LocalDateTime.parse(fieldsNode.get("created").textValue(), format));

        saveVersion(fieldsNode.get("fixVersions"), issue, "fix");
        saveVersion(fieldsNode.get("versions"), issue, "affected");

        issue.setDescription(getDescription(fieldsNode.get("description")));

        issue.setTimeoriginalestimate(fieldsNode.get("timeoriginalestimate").longValue());
        issue.setTimespent(fieldsNode.get("timespent").longValue());

        if (fieldsNode.get("parent") != null) {
            issue.setParentId(fieldsNode.get("parent").get("id").asInt());
            issue.setParentKey(fieldsNode.get("parent").get("key").asText());
        }
        return issue;
    }


    private String getDescription(JsonNode node) {
        StringBuilder stringBuilder = new StringBuilder();
        if (!node.isEmpty()) {
            return addString(node, stringBuilder).toString();
        }
        return null;
    }

    private void saveVersion(JsonNode node, JIssue jIssue, String type) {
        if (!node.isEmpty()) {
            List<JVersion> pojos = objectMapper.convertValue(
                    node,
                    new TypeReference<List<JVersion>>() {
                    });
            List<VersionIssue> versionIssues = new ArrayList<>();
            for (JVersion pojo : pojos) {
                if (!versionIssueRepository.existsByIssueIdAndLinkTypeAndVersionId(jIssue.getId(), type, pojo.getId()))
                    versionIssues.add(new VersionIssue(pojo, jIssue, type, jProject.getProjectKey()));
                if (!versionRepository.existsById(pojo.getId())) {
                    pojo.setProjectKey(jProject.getProjectKey());
                    versionRepository.save(pojo);
                } else {
                    JVersion jVersion = versionRepository.findById(pojo.getId()).get();
                    jVersion.setArchived(pojo.getArchived());
                    jVersion.setDescription(pojo.getDescription());
                    jVersion.setName(pojo.getName());
                    jVersion.setReleased(pojo.getReleased());
                }
            }
            versionIssueRepository.saveAll(versionIssues);
        }
    }

    public StringBuilder addString(JsonNode node, StringBuilder stringBuilder) {
        if (node.get("type").textValue().equalsIgnoreCase("mention")) {
            stringBuilder.append(node.get("attrs").get("text").textValue());
            stringBuilder.append(" (mention)");
        }
        if (node.has("text")) {
            stringBuilder.append(node.get("text").textValue() + " ");
        }
        if (node.has("content") && node.get("content").isArray()) {
            for (JsonNode content : node.get("content")) {
                addString(content, stringBuilder);
            }
        }
        return stringBuilder;
    }

    public List<IssueWithLabels> getIssuesByProjectAndUpdatedAndStatusIsNotIn(String projectKey, LocalDateTime isAfter, List<String> statuses ) {
        List<IssueWithLabels> issueDtos = new ArrayList<>();
        List<JIssue> jIssueList = issueRepository.findAllByProjectKeyAndUpdatedAfterAndStatusIsNotIn(projectKey, isAfter, statuses);
        if (!jIssueList.isEmpty()) {
            for (JIssue jIssue : jIssueList) {
                issueDtos.add(new IssueWithLabels(jIssue));
            }
        }
        log.info(issueDtos.size() + " issue dtos were created");
        return issueDtos;
    }

    public List<IssueWithLabels> getIssuesByProject(String projectKey) {
        List<IssueWithLabels> issueDtos = new ArrayList<>();
        List<JIssue> jIssueList = issueRepository.findAllByProjectKey(projectKey);
        if (!jIssueList.isEmpty()) {
            for (JIssue jIssue : jIssueList) {
                issueDtos.add(new IssueWithLabels(jIssue));
            }
        }
        log.info(issueDtos.size() + " issue dtos were created");
        return issueDtos;
    }

    public List<LabelDto> getAllLabelsForReport(String report) {
        List<LabelDto> dtos = new ArrayList<>();
        for (JProject project : projectRepository.findAllByReport(report)) {
            dtos.addAll(getProjectLabels(project.getProjectKey()));
        }
        return dtos;
    }


    public List<LabelDto> getProjectLabels(String projectKey) {
        List<LabelDto> dtos = new ArrayList<>();
        try {
            Optional<JProject> optionalJProject = projectRepository.findByProjectKey(projectKey);
            if (optionalJProject.isPresent()) {
                JProject project = optionalJProject.get();
                List<JIssue> jIssueList = issueRepository.findAllByProjectKey(projectKey);
                for (JIssue jIssue : jIssueList) {
                    if (!jIssue.getLabels().isEmpty()) {
                        for (String label : jIssue.getLabels()) {
                            dtos.add(new LabelDto(jIssue.getId(), label));
                        }
                    }
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        log.info(dtos.size() + " labels were send");
        return dtos;
    }
}



