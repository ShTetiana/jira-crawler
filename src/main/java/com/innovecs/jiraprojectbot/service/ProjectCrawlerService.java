package com.innovecs.jiraprojectbot.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecs.jiraprojectbot.model.JProject;
import com.innovecs.jiraprojectbot.model.dto.ProjectApiDto;
import com.innovecs.jiraprojectbot.repository.ProjectRepository;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.sun.istack.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
@RequiredArgsConstructor
public class ProjectCrawlerService {

    private final ObjectMapper objectMapper;
    private final ProjectRepository projectRepository;


    @Value("${jira.auth.user}")
    @NotNull
    private String user;

    @Value("${jira.auth.pswd}")
    @NotNull
    private String pswd;


    public void save(ProjectApiDto dto) {
        Optional<JProject> optionalJProject = projectRepository.findByProjectKey(dto.getProject());
        if (optionalJProject.isPresent()) {
            JProject project = optionalJProject.get();
            project.setDomain(dto.getDomain());
            projectRepository.save(project);
        }
    }

    public void crawlAvailableProjects(String domain) {
        try {
            List<JProject> projectList = new ArrayList<>();

            String url = "https://" + domain + ".atlassian.net/rest/api/3/project";// + counter;

            HttpResponse<String> response = Unirest.get(url)
                    .basicAuth(user, pswd)
                    .header("Accept", "application/json")
                    .asString();

            JsonNode parentNode = objectMapper.readTree(response.getBody());
            if (parentNode.isArray()) {
                log.info("Received: " + parentNode.size() + " records about projects");
                for (JsonNode node : parentNode) {
                    Integer projectId = Integer.parseInt(node.get("id").textValue());
                    String projectKey = node.get("key").textValue();
                    //if(!projectRepository.existsByIdAndProjectKey(projectId, projectKey)){
                        JProject project = new JProject();
                        project.setDomain(domain);
                        project.setId(projectId);
                        project.setProjectKey(projectKey);
                        project.setName(node.get("name").textValue());
                        projectList.add(project);
                   // }
                }
            }
            if(!projectList.isEmpty())
            {
                projectRepository.saveAll(projectList);
                log.info(projectList.size() + " new projects have been saved");
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }
}
