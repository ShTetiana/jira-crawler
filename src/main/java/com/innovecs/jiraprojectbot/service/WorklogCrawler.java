package com.innovecs.jiraprojectbot.service;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecs.jiraprojectbot.model.JIssue;
import com.innovecs.jiraprojectbot.model.JProject;
import com.innovecs.jiraprojectbot.model.JWorklog;
import com.innovecs.jiraprojectbot.repository.IssueRepository;
import com.innovecs.jiraprojectbot.repository.ProjectRepository;
import com.innovecs.jiraprojectbot.repository.WorklogRepository;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.sun.istack.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
@RequiredArgsConstructor
public class WorklogCrawler {

    private final ProjectRepository projectRepository;
    private final IssueRepository issueRepository;
    private final WorklogRepository worklogRepository;
    private final ObjectMapper objectMapper;

    private static JProject jProject;

    @Value("${jira.auth.user}")
    @NotNull
    private String user;

    @Value("${jira.auth.pswd}")
    @NotNull
    private String pswd;


    public void crawlWorklogForModifiedIssuesForDay(String projectKey){

        LocalDateTime date = LocalDateTime.now()
                .minusDays(1)
                .withHour(0)
                .withMinute(0)
                .withSecond(0);
        jProject = projectRepository.findByProjectKey(projectKey).get();
        List<JIssue> jIssueList = issueRepository
                .findAllByProjectKeyAndUpdatedAfter(projectKey, date);
        long worklogSize = worklogRepository.count();
        if(!jIssueList.isEmpty()) {
            for (JIssue jIssue : jIssueList) {
                getWorklogsByIssue(jIssue);
            }
            log.info("Total new worklogs: " + (worklogRepository.count() - worklogSize));
        } else log.info("There is no new issues for this project");



    }

    public void crawlWorklogsByProject(String projectKey) {
        worklogRepository.deleteAllByProjectKey(projectKey);
        Optional<JProject> jProjectOptional = projectRepository.findByProjectKey(projectKey);
        long worklogSize = worklogRepository.count();
        if (jProjectOptional.isPresent()) {
            jProject = jProjectOptional.get();
            List<JIssue> JIssues = issueRepository.findAllByProjectKey(jProject.getProjectKey());
            if (!JIssues.isEmpty()) {
                for (JIssue JIssue : JIssues) {
                    getWorklogsByIssue(JIssue);
                }
                log.info("Total new worklogs: " + (worklogRepository.count() - worklogSize));
            }
        }
    }

    // todo delete
    public void getLimitedWorklogs(String projectKey) {
        worklogRepository.deleteAllByProjectKey(projectKey);
        long worklogSize = worklogRepository.count();
        Optional<JProject> jProjectOptional = projectRepository.findByProjectKey(projectKey);
        if (jProjectOptional.isPresent()) {
            jProject = jProjectOptional.get();
            List<JIssue> JIssues = issueRepository.findAllByProjectKey(jProject.getProjectKey()).subList(0, 500);
            if (!JIssues.isEmpty()) {
                for (JIssue JIssue : JIssues) {
                    getWorklogsByIssue(JIssue);
                }
                log.info("Total new worklogs: " + (worklogRepository.count()-worklogSize));
            }
        }
    }

    public void getWorklogsByIssue(JIssue jissue) {
        List<JWorklog> jWorklogList = new ArrayList<>();
        int counter = 0;

        while (true) {
            try {
                String url = "https://"+ jProject.getDomain() +".atlassian.net/rest/api/3/issue/" + jissue.getKey() + "/worklog";// + counter;
                HttpResponse<String> response = Unirest.get(url)
                        .basicAuth(user, pswd)
                        .header("Accept", "application/json")
                        .queryString("startAt", counter)
                        .asString();
                JsonNode parentNode = objectMapper.readTree(response.getBody()).get("worklogs");
                if (parentNode.isArray()) {
                    if (parentNode.isEmpty()) {
                        break;
                    }
                    log.info("Received: " + parentNode.size() + " records about Worklogs of " + jissue.getKey());
                    for (JsonNode node : parentNode) {
                        JWorklog worklog = getWorklog(node);
                        worklog.setProjectKey(jissue.getProjectKey());
                        jWorklogList.add(worklog);
                    }
                }
                counter += 50;
                Thread.sleep(3000);
            } catch (Exception e) {
                System.out.println("Issue problem: " + jissue.getKey());
                e.printStackTrace();
            }
        }

        if (!jWorklogList.isEmpty()) {
            worklogRepository.saveAll(jWorklogList);
        }
    }


    private JWorklog getWorklog(JsonNode node) {
        JWorklog jWorklog = new JWorklog();

        jWorklog.setSelfLink(node.get("self").textValue());
        jWorklog.setId(Integer.parseInt(node.get("id").textValue()));
        jWorklog.setIssueId(Integer.parseInt(node.get("issueId").textValue()));

        jWorklog.setAuthorId(node.get("author").get("accountId").textValue());
        jWorklog.setAuthorName(node.get("author").get("displayName").textValue());

        jWorklog.setUpdateAuthorId(node.get("updateAuthor").get("accountId").textValue());
        jWorklog.setUpdateAuthorName(node.get("updateAuthor").get("displayName").textValue());

        jWorklog.setTimeSpent(node.get("timeSpent").textValue());
        jWorklog.setTimeSpentSeconds(node.get("timeSpentSeconds").intValue());

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        jWorklog.setUpdated(LocalDate.parse(node.get("updated").textValue(), format));
        jWorklog.setCreated(LocalDate.parse(node.get("created").textValue(), format));
        jWorklog.setStarted(LocalDate .parse(node.get("started").textValue(), format));

        return jWorklog;
    }

}
