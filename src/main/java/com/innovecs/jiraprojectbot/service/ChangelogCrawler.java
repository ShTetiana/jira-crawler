package com.innovecs.jiraprojectbot.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecs.jiraprojectbot.model.JChangelog;
import com.innovecs.jiraprojectbot.model.JIssue;
import com.innovecs.jiraprojectbot.model.JProject;
import com.innovecs.jiraprojectbot.repository.ChangelogRepository;
import com.innovecs.jiraprojectbot.repository.IssueRepository;
import com.innovecs.jiraprojectbot.repository.ProjectRepository;
import com.innovecs.jiraprojectbot.repository.WorklogRepository;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Log4j2
@Service
@RequiredArgsConstructor
public class ChangelogCrawler {

    @Value("${jira.auth.user}")
    @NotNull
    private String user;

    @Value("${jira.auth.pswd}")
    @NotNull
    private String pswd;

    private final ProjectRepository projectRepository;
    private final ChangelogRepository changelogRepository;
    private final IssueRepository issueRepository;
    private final ObjectMapper objectMapper;

    public List<JChangelog> getAllChangelogForReport(String report){
        List<JChangelog> changelogs = new ArrayList<>();
        for (JProject project : projectRepository.findAllByReport(report)) {
            changelogs.addAll(getChangelogOfProject(project.getProjectKey()));
        }
        return  changelogs;
    }

public List<JChangelog> getChangelogOfProject(String projectKey){
    List<JChangelog> list = changelogRepository.findAll()
            .stream().filter(item -> item.getProjectKey().equals(projectKey)).collect(Collectors.toList());
    log.info("size: " + list.size());
    return list;
}

    public void parseAllChangelogOfProject(String projectKey) {
        List<JIssue> list = issueRepository.findAllByProjectKey(projectKey);
        log.info("start " + projectKey);
        for (JIssue jIssue : list) {
            getIssueChangelog(jIssue);
        }

    }


    public void getIssueChangelog(JIssue issue) {
        try {
            List<JChangelog> changelogs = new ArrayList<>();
            String domain = projectRepository.findByProjectKey(issue.getProjectKey()).get().getDomain();
            HttpResponse<String> response = Unirest.get("http://"+domain+".atlassian.net/rest/api/3/issue/" + issue.getKey()+ "/changelog")
                    .basicAuth(user, pswd)
                    .header("Accept", "application/json")
                    .asString();
            JsonNode parentNode = objectMapper.readTree(response.getBody()).get("values");
            if (parentNode.isArray()) {
                log.info("Received: " + parentNode.size() + " records about Changelog of " + issue.getKey());
                for (JsonNode node : parentNode) {

                    Integer id = Integer.parseInt(node.get("id").textValue());
//                    log.info(node.get("id").intValue());
                    String authorId = node.get("author").get("accountId").textValue();
                    String authorName = node.get("author").get("displayName").textValue();
                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                    LocalDateTime created = LocalDateTime.parse(node.get("created").textValue(), format);
                    for (JsonNode itemNode : node.get("items")) {
                        String action = itemNode.get("field").textValue();
                        JChangelog changelog = new JChangelog(id, authorId, authorName, created, action, issue);
                        if (itemNode.get("from") != null) {
                            changelog.setFromId(itemNode.get("from").textValue());
                            changelog.setFromString(itemNode.get("fromString").textValue());
                        }
                        if (itemNode.get("to") != null) {
                            changelog.setToId(itemNode.get("to").asText());
                            changelog.setToString(itemNode.get("toString").textValue());
                        }
                        changelogs.add(changelog);
                    }

                    changelogRepository.saveAll(changelogs);


                }
            }
            Thread.sleep(3000);

        } catch (Exception e) {
            System.out.println("Issue problem: " + issue.getKey());
            e.printStackTrace();
        }


    }
}
