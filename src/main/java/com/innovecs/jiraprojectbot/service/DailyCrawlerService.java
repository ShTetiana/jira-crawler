package com.innovecs.jiraprojectbot.service;

import com.innovecs.jiraprojectbot.model.JProject;
import com.innovecs.jiraprojectbot.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
@EnableScheduling
@RequiredArgsConstructor
public class DailyCrawlerService {

    private final UserCrawler userCrawler;
    private final IssueCrawler issueCrawler;
    private final WorklogCrawler worklogCrawler;
    private final ProjectRepository projectRepository;


    @Scheduled(cron = "0 24 2 ? * *")
    public void dailyUpd() {
        List<String> projectKeys = projectRepository.findAll()
                .stream().filter(JProject::getDailyCrawled).map(JProject::getProjectKey).collect(Collectors.toList());
        for (String projectKey : projectKeys) {
            crawl(projectKey);
        }
    }



    private void crawl(String projectKey) {
        log.info("start of crawling");

        log.info("users:");
        userCrawler.crawlUsersForProject(projectKey);

        log.info("modified issues:");
        issueCrawler.crawlIssuesByProjectForDay(projectKey);

        log.info("worklog for modified issues:");
        worklogCrawler.crawlWorklogForModifiedIssuesForDay(projectKey);

        log.info("finish of crawling");
    }


}
