package com.innovecs.jiraprojectbot.service;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecs.jiraprojectbot.model.JProject;
import com.innovecs.jiraprojectbot.model.JUser;
import com.innovecs.jiraprojectbot.repository.ProjectRepository;
import com.innovecs.jiraprojectbot.repository.UserRepository;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.sun.istack.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
@RequiredArgsConstructor
public class UserCrawler {

    private final UserRepository userRepository;
    private final ObjectMapper objectMapper;
    private final ProjectRepository projectRepository;

    @Value("${jira.auth.pswd}")
    @NotNull
    private String pswd;

    @Value("${jira.auth.user}")
    @NotNull
    private String user;


    public List<JUser> getUsersForReport(String report){
        List<JProject> jProjects = projectRepository.findAllByReport(report);
        Set<JUser> jUsers = new HashSet<>();
        for (JProject jProject : jProjects) {
            jUsers.addAll(getUsersByProject(jProject.getProjectKey()));
        }
        return new ArrayList<>(jUsers);
    }


    public void crawlUsersForProject(String projectKey) {
        Optional<JProject> jProjectOptional = projectRepository.findByProjectKey(projectKey);
        if (jProjectOptional.isPresent()) {
            JProject project = jProjectOptional.get();
            List<JUser> users = new ArrayList<>();

            try {
                String url = "https://" + project.getDomain() + ".atlassian.net/rest/api/3/user/assignable/multiProjectSearch";// + counter;

                HttpResponse<String> response = Unirest.get(url)
                        .basicAuth(user, pswd)
                        .header("Accept", "application/json")
                        .queryString("projectKeys", project.getProjectKey())
                        .asString();
                JsonNode parentNode = objectMapper.readTree(response.getBody());
                if (parentNode.isArray()) {
                    log.info("Received: " + parentNode.size() + " records about Users");
                    for (JsonNode node : parentNode) {
                        // check if user already exist
                        String accountId = node.get("accountId").textValue();
                        JUser user = new JUser();
                        if (userRepository.existsById(accountId)) {
                            user = userRepository.findById(accountId).get();
                        } else {
                            user.setAccountId(accountId);
                        }
                        user.setEmail(node.get("emailAddress").textValue());
                        user.setDisplayName(node.get("displayName").textValue());
                        user.setActive(node.get("active").booleanValue());
                        user.setSelfLink(node.get("self").textValue());
                        user.setAccountType(node.get("accountType").textValue());
                        users.add(user);
                        if (project.getUsers().add(accountId)) {
                            log.info("new user has been saved");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            projectRepository.save(project);
            if (!users.isEmpty()) {
                userRepository.saveAll(users);
                log.info(users.size() + " users are saved");
            }
        } else {
            log.warn("This project is absent in table");
        }
    }


    public List<JUser> getUsersByProject(String projectKey) {
        List<JUser> jUsers = new ArrayList<>();
        Optional<JProject> jProjectOptional = projectRepository.findByProjectKey(projectKey);
        if (jProjectOptional.isPresent()) {
            Set<String> userIds = jProjectOptional.get().getUsers();
            if (!userIds.isEmpty()) {
                jUsers = userRepository.findAllById(userIds);
                log.info(jUsers.size() + " users were retrieved");
            } else log.warn("There is no user on this project");
        } else log.warn("This project is absent in table");
        return jUsers;
    }


}
