package com.innovecs.jiraprojectbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JiraProjectBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiraProjectBotApplication.class, args);
    }

}
