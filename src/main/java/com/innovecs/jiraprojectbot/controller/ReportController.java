package com.innovecs.jiraprojectbot.controller;

import com.innovecs.jiraprojectbot.model.JChangelog;
import com.innovecs.jiraprojectbot.model.JUser;
import com.innovecs.jiraprojectbot.model.dto.IssueWithLabels;
import com.innovecs.jiraprojectbot.model.dto.LabelDto;
import com.innovecs.jiraprojectbot.service.ChangelogCrawler;
import com.innovecs.jiraprojectbot.service.IssueCrawler;
import com.innovecs.jiraprojectbot.service.UserCrawler;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.*;

@Log4j2
@RestController
@RequiredArgsConstructor
public class ReportController {

    private final IssueCrawler issueCrawler;
    private final UserCrawler userCrawler;
    private final ChangelogCrawler changelogCrawler;
    private final JdbcTemplate template;


    @GetMapping("/get/{report}/users")
    public List<JUser> getUsers(@PathVariable(name = "report") String report)  {
        long start = new Date().getTime();
        log.info("get list of users request");

        List<JUser> users = userCrawler.getUsersForReport(report);
        log.info("get list of users before exit seconds: " + (new Date().getTime() - start) / 1000);
        return users;
    }


    @GetMapping("/get/{report}/versions")
    public List<Map<String, Object>> getVersions(@PathVariable(name = "report") String report) {
        long start = new Date().getTime();
        log.info("get list of versions request");

        List<Map<String, Object>> maps = template.queryForList("select * from j_version where project_key in (select project_key from j_project where j_project.report = '"+report+"')");
        log.info("get list of versions before exit seconds: " + (new Date().getTime() - start) / 1000);
        return maps;
    }


    @GetMapping("/get/{report}/issues")
    public List<IssueWithLabels> getIssues(@PathVariable(name = "report") String report) {
        long start = new Date().getTime();
        log.info("get list of issues request");

        List<IssueWithLabels> list = issueCrawler.getAllIssuesForReport(report);
        log.info("get list of issues before exit seconds: " + (new Date().getTime() - start) / 1000);
        return list;
    }



    @GetMapping("/get/{report}/issues/{isAfter}/exclude/{statuses}")
    public List<IssueWithLabels> getIssues(@PathVariable(name = "report") String report,
                                           @PathVariable(name = "isAfter") LocalDate date,
                                           @PathVariable(name = "statuses") List<String> statuses) {
        long start = new Date().getTime();
        log.info("get list of issues request");

        List<IssueWithLabels> list = issueCrawler.getAllIssuesForReportByDateIsAfterAndStatusIsNotIn(report, date.atStartOfDay(), statuses);
        log.info("get list of issues before exit seconds: " + (new Date().getTime() - start) / 1000);
        return list;
    }


    @GetMapping("/get/{report}/issue/versions")
    public List<Map<String, Object>> getIssueVersions(@PathVariable(name = "report") String report) {
        long start = new Date().getTime();
        log.info("get list of issue versions request");

        List<Map<String, Object>> maps = template.queryForList("select * from j_version_issue where project_key in (select project_key from  j_project where j_project.report = '"+report+"') ;");
        log.info("get list of issue versions before exit seconds: " + (new Date().getTime() - start) / 1000);
        return maps;
    }


    @GetMapping("/get/{report}/worklogs")
    public List<Map<String, Object>> getWorklogs(@PathVariable(name = "report") String report) {
        long start = new Date().getTime();
        log.info("get list of worklogs request");

        List<Map<String, Object>> maps = template.queryForList("select * from j_worklog where project_key in ( select project_key from j_project where report = '"+report+"');");
        log.info("get list of worklogs before exit seconds: " + (new Date().getTime() - start) / 1000);
        return maps;
    }

    @GetMapping("/get/{report}/issue/labels")
    public List<LabelDto> getProjectIssueLabels(@PathVariable(name = "report") String report) {
        long start = new Date().getTime();
        log.info("get list of issue labels request");
        List<LabelDto> dtos = issueCrawler.getAllLabelsForReport(report);
        log.info("get list of issue labels before exit seconds: " + (new Date().getTime() - start) / 1000);
        return dtos;
    }

    @PostMapping("/get/{report}/issue/changelog")
    @ResponseBody
    public List<JChangelog> getUCChangelog(@PathVariable(name = "report") String report){
        log.info("list of changelog");
        List<JChangelog> list = changelogCrawler.getAllChangelogForReport(report);
        log.info("total size: " + list.size());
        return list;
    }


}
