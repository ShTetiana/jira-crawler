package com.innovecs.jiraprojectbot.controller;

import com.innovecs.jiraprojectbot.model.JChangelog;
import com.innovecs.jiraprojectbot.service.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequiredArgsConstructor
public class CrawlerController {

    private final IssueCrawler issueCrawler;
    private final UserCrawler userCrawler;
    private final WorklogCrawler worklogCrawler;
    private final ProjectCrawlerService projectCrawler;
    private final DailyCrawlerService dailyCrawlerService;
    private final ChangelogCrawler changelogCrawler;
    private final JdbcTemplate template;



    @GetMapping("/crawl/{domain}/projects")
    public String parse(@PathVariable(name = "domain") String domain) {
        try {
            log.info("crawling is started");
            projectCrawler.crawlAvailableProjects(domain);
            log.info("crawling is finished");
        } catch (Exception ex) {
            return "error. Watch logs";
        }
        return "crawling is finished";
    }



    @GetMapping("/crawl/{projectKey}/users")
    public String parseUsers(@PathVariable(name = "projectKey") String projectKey) {
        try {
            log.info("crawling is started");
            userCrawler.crawlUsersForProject(projectKey);
            log.info("crawling is finished");
        } catch (Exception ex) {
            return "error. Watch logs";
        }
        return "crawling is finished";
    }

    @GetMapping("/crawl/{projectKey}")
    public String parseFullProjectData(@PathVariable(name = "projectKey") String projectKey) {
        try {
            log.info("crawling is started");
            userCrawler.crawlUsersForProject(projectKey);
            issueCrawler.crawlAllIssueByProject(projectKey);
            worklogCrawler.crawlWorklogsByProject(projectKey);
            changelogCrawler.parseAllChangelogOfProject(projectKey);
            log.info("crawling is finished");
        } catch (Exception ex) {
            return "error. Watch logs";
        }
        return "crawling is finished";
    }


    @GetMapping("/crawl/{projectKey}/issues")
    public String parseIssues(@PathVariable(name = "projectKey") String projectKey) {
        try {
            log.info("crawling is started");
            issueCrawler.crawlAllIssueByProject(projectKey);
            log.info("crawling is finished");
        } catch (Exception ex) {
            return "error. Watch logs";
        }
        return "crawling is finished";
    }


    @GetMapping("/crawl/{projectKey}/worklogs")
    public String parseWorklogs(@PathVariable(name = "projectKey") String projectKey) {
        try {
            log.info("crawling is started");
            worklogCrawler.crawlWorklogsByProject(projectKey);
            log.info("crawling is finished");
        } catch (Exception ex) {
            return "error. Watch logs";
        }
        return "crawling is finished";
    }

    @GetMapping("/crawl/{projectKey}/worklogs/limited")
    public String parseLimitedWorklogs(@PathVariable(name = "projectKey") String projectKey) {
        try {
            log.info("crawling is started");
            worklogCrawler.getLimitedWorklogs(projectKey);
            log.info("crawling is finished");
        } catch (Exception ex) {
            return "error. Watch logs";
        }
        return "crawling is finished";
    }

    @GetMapping("/crawl/{projectKey}/issues/changelog")
    public String parseChangelogs(@PathVariable(name = "projectKey") String projectKey) {
        try {
            log.info("crawling is started");
            changelogCrawler.parseAllChangelogOfProject(projectKey);
            log.info("crawling is finished");
        } catch (Exception ex) {
            return "error. Watch logs";
        }
        return "crawling is finished";
    }

    @GetMapping("/crawl/all/daily")
    public String parseDailyProjectsData() {
        try {
            log.info("crawling is started");
            dailyCrawlerService.dailyUpd();
            log.info("crawling is finished");
        } catch (Exception ex) {
            return "error. Watch logs";
        }
        return "crawling is finished";
    }




}
