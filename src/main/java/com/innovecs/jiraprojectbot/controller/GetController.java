package com.innovecs.jiraprojectbot.controller;

import com.innovecs.jiraprojectbot.model.JUser;
import com.innovecs.jiraprojectbot.model.dto.IssueWithLabels;
import com.innovecs.jiraprojectbot.model.dto.LabelDto;
import com.innovecs.jiraprojectbot.service.IssueCrawler;
import com.innovecs.jiraprojectbot.service.UserCrawler;
import com.innovecs.jiraprojectbot.service.WorklogCrawler;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Log4j2
@RestController
@RequiredArgsConstructor
public class GetController {

    private final IssueCrawler issueCrawler;
    private final UserCrawler userCrawler;
    private final JdbcTemplate template;


    @GetMapping("/get/pr/{projectKey}/users")
    public List<JUser> getUsers(@PathVariable(name = "projectKey") String projectKey) {
        long start = new Date().getTime();
        log.info("get list of users request");

        List<JUser> users = userCrawler.getUsersByProject(projectKey);
        log.info("get list of users before exit seconds: " + (new Date().getTime() - start) / 1000);
        return users;
    }


    @GetMapping("/get/pr/{projectKey}/versions")
    public List<Map<String, Object>> getVersions(@PathVariable(name = "projectKey") String projectKey) {
        long start = new Date().getTime();
        log.info("get list of versions request");

        List<Map<String, Object>> maps = template.queryForList("select * from j_version where project_key = '"+ projectKey + "';");
                log.info("get list of versions before exit seconds: " + (new Date().getTime() - start) / 1000);
        return maps;
    }


    @GetMapping("/get/pr/{projectKey}/issues")
    public List<IssueWithLabels> getIssues(@PathVariable(name = "projectKey") String projectKey) {
        long start = new Date().getTime();
        log.info("get list of issues request");

        List<IssueWithLabels> list = issueCrawler.getIssuesByProject(projectKey);
        log.info("get list of issues before exit seconds: " + (new Date().getTime() - start) / 1000);
        return list;
    }


    @GetMapping("/get/pr/{projectKey}/issue/versions")
    public List<Map<String, Object>> getIssueVersions(@PathVariable(name = "projectKey") String projectKey) {
        long start = new Date().getTime();
        log.info("get list of issue versions request");

        List<Map<String, Object>> maps = template.queryForList("select * from j_version_issue where project_key = '"+ projectKey + "';" );
        log.info("get list of issue versions before exit seconds: " + (new Date().getTime() - start) / 1000);
        return maps;
    }


    @GetMapping("/get/pr/{projectKey}/worklogs")
    public List<Map<String, Object>> getWorklogs(@PathVariable(name = "projectKey") String projectKey) {
        long start = new Date().getTime();
        log.info("get list of worklogs request");

        List<Map<String, Object>> maps = template.queryForList("select * from j_worklog where project_key = '"+ projectKey + "';");
        log.info("get list of worklogs before exit seconds: " + (new Date().getTime() - start) / 1000);
        return maps;
    }

    @GetMapping("/get/pr/{projectKey}/issue/labels")
    public List<LabelDto> getProjectIssueLabels(@PathVariable(name = "projectKey") String projectKey) {
        long start = new Date().getTime();
        log.info("get list of issue labels request");

        List<LabelDto> dtos = issueCrawler.getProjectLabels(projectKey);
        log.info("get list of issue labels before exit seconds: " + (new Date().getTime() - start) / 1000);
        return dtos;
    }
}
