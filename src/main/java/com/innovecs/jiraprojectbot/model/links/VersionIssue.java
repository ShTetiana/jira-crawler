package com.innovecs.jiraprojectbot.model.links;

import com.innovecs.jiraprojectbot.model.JIssue;
import com.innovecs.jiraprojectbot.model.JVersion;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "j_version_issue")
@NoArgsConstructor
public class VersionIssue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer issueId;

    private String issueKey;

    private Integer versionId;

    private String linkType;

    private String projectKey;

    public VersionIssue(JVersion JVersion, JIssue JIssue, String type, String projectKey) {
        this.issueId = JIssue.getId();
        this.issueKey = JIssue.getKey();
        this.versionId = JVersion.getId();
        this.linkType = type;
        this.projectKey = projectKey;
    }

}
