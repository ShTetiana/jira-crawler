package com.innovecs.jiraprojectbot.model;

import lombok.Data;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Transactional
@Table(name = "j_project")
@IdClass(JProjectId.class)
public class JProject {

    @Id
    private Integer id;

    @Id
    private String projectKey;

    private String name;

    private String domain;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "j_project_users")
    Set<String> users = new HashSet<>();

    private Boolean dailyCrawled;
    private String report;

}
