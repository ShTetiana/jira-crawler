package com.innovecs.jiraprojectbot.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "j_issue")
@NoArgsConstructor
@AllArgsConstructor
public class JIssue {

    private String selfLink;

    @Id
    private Integer id;

    private String key;

    private String priority;

    private Integer priorityId;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "j_issue_labels")
    private List<String> labels;

    private String assigneeId;

    private String status;

    private Integer statusId;

    private String creatorId;

    private String reporterId;

    private Integer typeId;

    private String type;

    private String projectKey;

    private Integer projectId;

    @Column(columnDefinition = "text")
    private String description;

    private String summary;

    private LocalDateTime updated;

    private LocalDateTime created;

    private Long timeoriginalestimate;

    private Long timespent;

    private Integer parentId;

    private String parentKey;
}
