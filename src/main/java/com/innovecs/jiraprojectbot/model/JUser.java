package com.innovecs.jiraprojectbot.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "j_user")
public class JUser {

    private String selfLink;

    @Id
    private String accountId;

    private String accountType;

    private String email;

    private String displayName;

    private Boolean active;


}
