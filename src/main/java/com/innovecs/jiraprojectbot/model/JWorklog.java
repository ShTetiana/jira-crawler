package com.innovecs.jiraprojectbot.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "j_worklog")
public class JWorklog {

    private String selfLink;

    @Id
    private Integer id;

    private Integer issueId;

    private String authorId;

    private String authorName;

    private String updateAuthorId;

    private String updateAuthorName;

    private LocalDate created;
    private LocalDate updated;
    private LocalDate started;

    private String timeSpent;
    private Integer timeSpentSeconds;

    private String projectKey;

}
