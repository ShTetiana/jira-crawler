package com.innovecs.jiraprojectbot.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JProjectId implements Serializable {

    private Integer id;

    private String projectKey;
}
