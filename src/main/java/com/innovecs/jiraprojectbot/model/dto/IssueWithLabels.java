package com.innovecs.jiraprojectbot.model.dto;

import com.innovecs.jiraprojectbot.model.JIssue;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class IssueWithLabels {

    private String selfLink;

    private Integer id;

    private String key;

    private String priority;

    private Integer priorityId;

    private String assigneeId;

    private String status;

    private Integer statusId;

    private String creatorId;

    private String reporterId;

    private Integer typeId;

    private String type;

    private String project;

    private Integer projectId;

    private String description;

    private String summary;

    private LocalDateTime updated;

    private LocalDateTime created;

    private Long timeoriginalestimate;

    private Long timespent;

    private Integer parentId;

    private String parentKey;

    public IssueWithLabels(JIssue jIssue){
        this(jIssue.getSelfLink(), jIssue.getId(),jIssue.getKey(),
                jIssue.getPriority(), jIssue.getPriorityId(),
                jIssue.getAssigneeId(), jIssue.getStatus(), jIssue.getStatusId(),
                jIssue.getCreatorId(), jIssue.getReporterId(),
                jIssue.getTypeId(), jIssue.getType(),
                jIssue.getProjectKey(), jIssue.getProjectId(),
                jIssue.getDescription(), jIssue.getSummary(),
                jIssue.getUpdated(), jIssue.getCreated(),
                jIssue.getTimeoriginalestimate(), jIssue.getTimespent(),
                jIssue.getParentId(), jIssue.getParentKey());
    }
}
