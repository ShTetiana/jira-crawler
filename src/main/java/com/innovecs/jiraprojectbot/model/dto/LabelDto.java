package com.innovecs.jiraprojectbot.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LabelDto {

    private Integer issueId;
    private String label;

}
