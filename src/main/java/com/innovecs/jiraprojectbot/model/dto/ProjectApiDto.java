package com.innovecs.jiraprojectbot.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class ProjectApiDto {

    private String project;

    private String domain;

}
