package com.innovecs.jiraprojectbot.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;


@JsonPropertyOrder({
        "self",
        "id",
        "description",
        "name",
        "archived",
        "released"
})
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Entity
@Table(name = "j_version")
public class JVersion {

    @Id
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("self")
    private String self;

    @JsonProperty("description")
    private String description;

    @JsonProperty("name")
    private String name;

    @JsonProperty("archived")
    private Boolean archived;

    @JsonProperty("released")
    private Boolean released;

    private String projectKey;

}
